::: G I F F E N   G O O D :::  (2014)  |  for trombone + live electronics
composed by Louis Goldford ::: for David Whitwell on de slide trampagne

(If experiencing any difficulty whatsoever, send me an email @ ljgoldford@gmail.com.)

/////////////////////////////////////////////////////////////////////////////////////

Software designed in + for Max/MSP v. 6.1.3+

***Please be sure you're running the latest version of Max!***
If you need an upgrade, visit http://cycling74.com/downloads/

(If running Max 5, see below under "Installation.") 
(Can also run on fee version Max Runtime from the above site.)

/////////////////////////////////////////////////////////////////////////////////////

::: ADDITIONAL LIBRARIES TO BE INSTALLED ::: (All bound by their original licenses.)

Please have each of the following installed properly on your machine…
before attempting to open or run the piece…

1. IRCAM's FTM & Co. for Max v6+ --> http://ftm.ircam.fr/index.php/Download
('Giffen Good' was composed w/ v2.6.0 BETA (09/2102), from the above site.)

2. Bach: Automated Composer's Helper --> http://www.bachproject.net/download
('Giffen Good' was composed w/ v0.7.1 BETA, from the above site.)

Install each of the above libraries using directions provided by each package.
If experiencing any difficulty, contact those developers directly.
Or send an email to ljgoldford@gmail.com. Hopefully I can help with that!!

/////////////////////////////////////////////////////////////////////////////////////

::: INSTALLATION OF THIS PACKAGE :::

Once unzipped, place the entire folder "goldford-giffen-good-2014" 
into your Max packages folder. Should be located here:

Macintosh HD:/Applications/Max 6.1/packages

Theoretically the piece also runs in Max 5. 
If you're attempting to run it on a version of Max prior to v6, 
place the folder somewhere in your Max search path instead. For example:

Macintosh HD:/Applications/Max/patches

****BUT!!****
If running one of these earlier versions of Max, BE ABSOLUTELY SURE you are also
running compatible earlier versions of the above ADDITIONAL LIBRARIES. See their
websites for obtaining earlier versions.

/////////////////////////////////////////////////////////////////////////////////////

::: LICENSES + ACKNOWLEDGEMENTS :::

Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International 
(CC BY-NC-ND 4.0) license

http://creativecommons.org/licenses/by-nc-nd/4.0/

This software is licensed under the Creative Commons 
Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0) license 
--> http://creativecommons.org/licenses/by-nc-nd/4.0/ 
with the exception of the aforementioned 'Additional Libraries,' 
the following external objects included in this package,
as well as an audio excerpt, outlined below.

All such additional tools are bound by their original licenses:

1. zsa.?? (which objects? put them here!!) 
from the zsa.descriptors external library 
Copyright © ???? Emmanuel Jourdan + Mikhail Malt
(website)

2. (audio excerpt from Dutch stock exchange)
(freesound.org page)
(disclaimer from user)

3. (Anything else I'm not remembering??????)

Additionally, I would like to extend my greatest thanks to the following artists, 
developers and instructors, whose expertise have played a enormous role in the design 
of my recent electroacoustic music, who taught me certain methods of synthesis and 
design which may be found here or in some of my other pieces:

Diemo Schwarz
Jean Lochard
Tom Mays